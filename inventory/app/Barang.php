<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';

    protected $fillable = ['nama','deskripsi', 'gambar', 'saldo_barang'];
    
    public function get_kategori()
    {
    	return $this->belongsToMany('App\Kategori','kategori_barang','barang_id','kategori_id');
    }
}

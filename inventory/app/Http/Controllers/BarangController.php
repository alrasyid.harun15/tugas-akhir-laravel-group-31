<?php

namespace App\Http\Controllers;

use App\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Alert;
use App\Kategori;
use Image;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $daftar_barang = Barang::all();

        return view('barang.list', ['daftar_barang' => $daftar_barang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

        $daftar_kategori = Kategori::all();
        
        return view('barang.create',['daftar_kategori'=>$daftar_kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:barang|max:100',
            'deskripsi' => 'required|max:6000',
            'gambar' => 'required|image|max:1024',
            'daftar_kategori' => 'required|array'
        ]);

        $file_image = $request->file('gambar');
        $filename = time() . '.' . $file_image->getClientOriginalExtension();
        $file_image->move('images',$filename);

        $barang = Barang::create([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi'],
            'gambar' => $filename
        ]);

        //dd($barang);

        //menyimpan many to many di tabel pivot
        $id_kategori = $request['daftar_kategori'];
        foreach ($id_kategori as $id) {
            $barang->get_kategori()->save(Kategori::find($id));
        }
        
        
        $message = 'Barang dengan nama "'. $barang->nama .'" berhasil disimpan!';

        Alert::success('Success Title', $message)->position('bottom-end');

        return redirect()->route('barang.index')->with('success',$message); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        $barang = Barang::find($barang->id);

        return view('barang.show', ['barang' => $barang]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        $barang = Barang::find($barang->id);

        $daftar_kategori = Kategori::all();

        $id_barang_terpilih = array();

        foreach ($barang->get_kategori as $key => $kategori) {

            $id_barang_terpilih[] = $kategori->id;

        }

        return view('barang.edit', ['barang' => $barang,
            'daftar_kategori' => $daftar_kategori,
            'id_barang_terpilih' => $id_barang_terpilih
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:barang|max:100',
            'deskripsi' => 'required|max:6000',
            'gambar' => 'required|image|max:1024',
            'daftar_kategori' => 'required|array'
        ]);

        $file_image = $request->file('gambar');
        $filename = time() . '.' . $file_image->getClientOriginalExtension();
        $file_image->move('images',$filename);

        $barang = Barang::find($barang->id);
        $barang->nama = $request['nama'];
        $barang->deskripsi = $request['deskripsi'];
        $barang->gambar = $filename;

        $barang->save();

        $barang->get_kategori()->detach();

        $id_kategori = $request['daftar_kategori'];
        foreach ($id_kategori as $id) {
            $barang->get_kategori()->save(Kategori::find($id));
        }

        $message = 'Barang dengan nama "'. $barang->nama .'" berhasil diubah!';

        Alert::success('Success Title', $message)->position('bottom-end');;
        
        return redirect()->route('barang.index')->with('success',$message); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        
        $barang=Barang::find($barang->id);

        $barang->get_kategori()->detach();

        Barang::destroy($barang->id);    


        $message = 'Barang dengan nama "'. $barang->nama .'" berhasil dihapus!';
        Alert::success('Success Title', $message)->position('bottom-end');;

        return redirect()->route('barang.index')->with('success',$message); 
    }
    
}

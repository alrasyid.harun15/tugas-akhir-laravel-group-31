<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Alert;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftar_kategori = Kategori::all();

        return view('kategori.list', ['daftar_kategori' => $daftar_kategori]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:kategori|max:100',            
        ]);

        $kategori = Kategori::create([
            'nama' => $request['nama']            
        ]);

        $message = 'Kategori "'. $kategori->nama .'" berhasil disimpan!';
        Alert::success('Success Title', $message)->position('bottom-end');

        return redirect()->route('kategori.index')->with('success',$message); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        $kategori = Kategori::find($kategori->id);

        return view('kategori.show', ['kategori' => $kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        $kategori = Kategori::find($kategori->id);

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:kategori|max:100',            
        ]);

        $kategori = Kategori::find($kategori->id);

        $kategori->nama = $request['nama'];
        $kategori->save();
        
        $message = 'Kategori "'. $kategori->nama .'" berhasil diubah!';
        Alert::success('Success Title', $message)->position('bottom-end');

        return redirect()->route('kategori.index')->with('success',$message); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        Kategori::destroy($kategori->id);

        $message = 'Kategori "'. $kategori->nama .'" berhasil dihapus!';
        Alert::success('Success Title', $message)->position('bottom-end');

        return redirect()->route('kategori.index')->with('success',$message); 
    }
}

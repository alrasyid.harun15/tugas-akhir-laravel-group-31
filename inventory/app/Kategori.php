<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';

    protected $fillable = ['nama'];
    
    public function get_barang()
    {
    	return $this->belongsToMany('App\Barang','kategori_barang','kategori_id','barang_id');
    }
    
}

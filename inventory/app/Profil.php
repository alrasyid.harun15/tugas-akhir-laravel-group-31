<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model{

    
    protected $table = 'profile';

    protected $fillable = ['id','nama_lengkap','no_telepon', 'alamat', 'bio', 'foto'];

}

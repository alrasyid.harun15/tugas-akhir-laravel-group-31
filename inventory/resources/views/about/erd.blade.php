@extends('adminlte/master')

@section('content')
<div class="container">
    <div class="row justify-content-center ">
        <div class="col-md-8">
            <div class="card mt-3">
                <div class="card-header">Aplikasi Inventori Kelompok 31</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Berikut ini adalah ERD kami
                    <img src="{{URL::to('/images/inventory.png')}}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

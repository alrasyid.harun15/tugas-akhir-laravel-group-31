@extends('adminlte/master')

@push('styles')

  <link rel="stylesheet" href="{{asset('/adminlte/plugins/select2/css/select2.min.css')}}">

@endpush

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Update Data Barang</h3>
      </div>
      <!-- /.card-header -->

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

      <!-- form start -->
      <form role="form" action="{{route('barang.update',['barang'=>$barang])}}" method="POST" enctype="multipart/form-data">
      	@method('PUT')
      	@csrf
        <div class="card-body">
          <div class="form-group">
            <label for="nama_id">Nama</label>
            <input type="text" class="form-control" id="nama_id" placeholder="Masukkan Nama" name="nama" value="{{old('nama',$barang->nama)}}">
            @error('isi')
  			       <div class="alert alert-danger">{{ $message }}</div>
		        @enderror
          </div>   

          <div class="form-group">
            <label for="isi_id">Deskripsi</label>
            <textarea id="deskripsi_id" class="deskripsi" name="deskripsi">{{old('deskripsi',$barang->deskripsi)}}</textarea>                        
             @error('deskripsi')
               <div class="alert alert-danger">{{ $message }}</div>
             @enderror
          </div>   

          <div class="form-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="gambar_id" placeholder="Pilih file gambar" name="gambar" value="{{old('gambar',$barang->gambar)}}">
              <label class="custom-file-label" for="gambar_id">Pilih File Gambar</label>
              @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror  
            </div> 
          </div>

          <div class="form-group">
            
            <label for="kategori_id" >Kategori</label>            

            <select class="js-example-basic-multiple" name="daftar_kategori[]" multiple="multiple" id="kategori_id" >
              
              @foreach ($daftar_kategori as $key => $kategori)
                <option value="{{$kategori->id}}"

                  {{in_array($kategori->id, $id_barang_terpilih)?'selected':''}}    

                  >{{$kategori->nama}}</option>
              @endforeach  

            </select>

             @error('daftar_kategori')
               <div class="alert alert-danger">{{ $message }}</div>
             @enderror  
          </div>     

          
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Ubah Jawaban</button>
        </div>
      </form>
    </div>

@endsection

@push('scripts')
    
  <script src="{{asset('/adminlte/plugins/select2/js/select2.min.js')}}"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

  <script type="text/javascript">
    
    $(document).ready(function() {
      $('.js-example-basic-multiple').select2();
    });

    $(document).ready(function () {
      bsCustomFileInput.init();
    });
  
  </script>

  <script>
      tinymce.init({
          selector:'textarea.deskripsi',
          width: 900,
          height: 300
      });
  </script>  
  

@endpush
@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Data Barang</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
        
        <div class="card ml-3 mr-3 mt-3 mb-3">
          <div class="card-body" >
            <h5 class="card-title">Nama : {{$barang->nama}}</h5>
            <p class="card-text">Deskripsi : {!! $barang->deskripsi !!}</p>
            <p class="card-text">
              Kategori : 
              <ul>
              @foreach ($barang->get_kategori as $key => $kategori)
                <li value="{{$kategori->id}}">{{$kategori->nama}}</li>
              @endforeach  
              </ul>  
            </p>
            <p class="card-image">
              <img src="{{URL::to('/images/')}}/{{$barang->gambar}}" class="img-thumbnail" width="300">
            </p>
            <a class="btn btn-primary" href="{{route('barang.edit',['barang'=>$barang])}}">Ubah Data Barang</a>
          </div>
        </div>

      
    </div>

@endsection
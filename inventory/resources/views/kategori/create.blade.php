@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Masukkan Kategori</h3>
      </div>
      <!-- /.card-header -->

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

      <!-- form start -->
      <form role="form" action="{{route('kategori.store')}}" method="POST">
      	@method('POST')
      	@csrf
        
        <div class="card-body">
          <div class="form-group">
            <label for="nama_id" >Kategori</label>
            <input type="text" class="form-control" id="kategori_id" placeholder="Masukkan nama kategori" name="nama" value="{{old('nama','')}}" >
             @error('nama')
    			     <div class="alert alert-danger">{{ $message }}</div>
			       @enderror	
          </div>        
          
          
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Simpan Ketegori Baru</button>
        </div>
      </form>
    </div>

@endsection
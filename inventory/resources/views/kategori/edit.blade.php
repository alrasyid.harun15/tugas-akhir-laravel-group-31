@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Ubah Kategori</h3>
      </div>
      <!-- /.card-header -->

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

      <!-- form start -->
      <form role="form" action="{{route('kategori.update',['kategori'=>$kategori])}}" method="POST">
      	@method('PUT')
      	@csrf
        <div class="card-body">
          <div class="form-group">
            <label for="nama_id">nama</label>
            <input type="text" class="form-control" id="nama_id" placeholder="Masukkan nama" name="nama" value="{{old('nama',$kategori->nama)}}">
            @error('isi')
  			       <div class="alert alert-danger">{{ $message }}</div>
		        @enderror
          </div>   
          
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Ubah Kategori</button>
        </div>
      </form>
    </div>

@endsection
@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Kategori</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
        
        <div class="card ml-3 mr-3 mt-3 mb-3">
          <div class="card-header">
            <h3 class="card-title">{{$kategori->nama}}</h3>
          </div>
          <div class="card-body" >
            <h5 class="card-title">Berikut dibawah Ini adalah daftar barang di kategori ini!</h5>
            <a class="btn btn-primary" href="{{route('kategori.edit',['kategori'=>$kategori])}}">Ubah Kategori</a>
          </div>
        </div>

        <div class="card">
      <div class="card-header">
        <h3 class="card-title">Daftar Barang dengan kategori : {{$kategori->nama}} </h3>
      </div>
      <!-- /.card-header -->      

      <div class="card-body">

        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Deskripsi</th>            
          </tr>
          </thead>
          <tbody>           
            @foreach ($kategori->get_barang as $key => $barang)          
            <tr>
                <td>{{$key+1}}</td> 
                <td>{{$barang->nama}}</td>
                <td>{!!$barang->deskripsi!!}</td>                
            </tr>
            @endforeach      
          </tbody>
          
        </table>
      </div>
      <!-- /.card-body -->
      </div>

      
    </div>

@endsection
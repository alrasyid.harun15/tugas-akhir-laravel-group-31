
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>	

	<h1>Daftar Barang Inventori 31</h1>
	<table border="1">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Deskripsi</th>
			<th>Kategori</th>
		</tr>
		@foreach ($items as $key => $item)
		<tr>
			<td>{{ ++$key }}</td>
			<td>{{ $item->nama }}</td>
			<td>{!! $item->deskripsi !!}</td>
			<td>
				<ul>
				@foreach ($item->get_kategori as $key_kategori => $kategori)	
					<li>{{$kategori->nama}}</li>
				@endforeach
				</ul>
			</td>
		</tr>
		@endforeach
	</table>

</body>
</html>

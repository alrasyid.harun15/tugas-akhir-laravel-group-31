@extends('adminlte.master')

@section('content')

<div class="container my-5">
                    

    <div class="data-karyawan">
        <h3 class="text-center">Profil</h3> 
        <div class="foto-user mx-auto my-5">
            <img src="{{asset('adminlte/dist/img/avatar.png')}}" class="img-fluid rounded-circle foto-user">
        </div>

        <table class="table-hover table-data table-edit my-5 mx-auto">
            <tbody>                 

                <tr>
                    <td> Nama Lengkap </td>
                    <td> : </td>
                    <td> {{Auth::user()->profile->nama_lengkap}} </td>
                   
                </tr>
{{-- 
                <tr>
                    <td> No Telepon  </td>
                    <td> : </td>
                    <td> {{Auth::user()->profile->no_telepon}} </td>
                    
                </tr>

                <tr>
                    <td> Alamat  </td>
                    <td> : </td>
                    <td> {{Auth::user()->profile->alamat}} </td>
                   
                </tr>

                <tr>
                    <td> Bio  </td>
                    <td> : </td>
                    <td> {{Auth::user()->profile->bio}} </td>
                    
                </tr> --}}

            </tbody>
        </table>

    </div>

    
    
</div>

@endsection

            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="{{asset('bootstrap/styles/main.css')}}">
                <link rel="stylesheet" href="{{asset('bootstrap/libraries/bootstrap/css/bootstrap.min.css')}}">
                <link href="https://fonts.googleapis.com/css2?family=Gentium+Book+Basic:ital,wght@0,400;0,700;1,400;1,700&family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
                <title> Login - 31 Inventory</title>
            </head>
            <body>
            
                <!-- header -->
                <div class="container">
                    <nav class="row navbar navbar-expand-lg navbar-brand">

                       

                        <a class="logo" href="{{ url('/home') }}"> 
                            <img src="{{asset('bootstrap/img/logo.png')}}">
                        </a>

                        @if (Route::has('login'))
                        @auth

                        <div class="btn-group" role="group" aria-label="Basic example ">
                            
                            @else
                            <a href="{{ route('login') }}" class="btn btn-primary buttonEdit">Login</a>

                            @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-outline-primary buttonEdit">Register</a>

                            @endif
                           
                            
                          </div>
                          @endauth
                          @endif 
                    </nav>
                </div>
            
                <!-- login -->
                <nav class="login-session">
                    <div class="container">
                        <section class="foto-login d-block">
                                <img src="{{asset('bootstrap/img/img_front.png')}}" class="img-fluid">
                        </section>
                    
                        <div class="login px-5">
                            <header class="text-center">     
                                <h2> Aplikasi</h2>
                                <h1>31 Inventory</h1>
                            </header>
                        </div>  
                    </div>
                </nav>
            
                <nav class="footer">
                    <footer>
                        <div class="container-fluid">
                            <div class="row justify-content-center align-items-center my-3">
                                <div class="col-auto text-gray-500 font-weight-light">
                                    Copyright, <strong> &copy;31 inventory - Group 31 - Laravel 18 </strong> 
                                </div>
                                    
                            </div>
                        </div>
                    </footer>
                </nav>
            
                    
                
            
                <script src="{{asset('bootstrap/libraries/bootstrap/js/bootstrap.js')}}"></script>
                <script src="{{asset('bootstrap/libraries/jquery/jquery-3.5.1.min.js')}}"></script>
                <script src="{{asset('bootstrap/libraries/retina/retina.min.js')}}"></script>
            </body>
            </html>
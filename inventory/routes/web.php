<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/print', 'ReportController@generate')->name('report');

Route::get('/profil', 'ProfilController@index')->name('profil');

Route::get('/transaksi', 'TransaksiController@index')->name('transaksi');

Route::get('/trs_detail', 'TransaksiDetailController@index')->name('trs_detail');





































# Harun 

Route::resource('barang', 'BarangController')->middleware('auth');

Route::resource('kategori', 'KategoriController')->middleware('auth');

Route::get('/print/barang', 'ReportController@generateBarangReport')->name('barang.pdf')->middleware('auth');


Route::get('/erd', function () {
    return view('about/erd');
})->middleware('auth');

Route::get('/group', function () {
    return view('about/group');
})->middleware('auth');